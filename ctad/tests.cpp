#include <algorithm>
#include <boost/type_index.hpp>
#include <iostream>
#include <list>
#include <numeric>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

template <typename T>
void deduce1(T arg)
{
    puts(__PRETTY_FUNCTION__);
}

template <typename T>
void deduce2(T& arg)
{
    puts(__PRETTY_FUNCTION__);
}

template <typename T>
void deduce3(T&& arg)
{
    puts(__PRETTY_FUNCTION__);

    auto x = std::forward<T>(arg);
}

void foo()
{
}

TEST_CASE("Template Argument Deduction - case 1")
{
    int x = 10;
    const int cx = 20;
    int& ref_x = x;
    const int& cref_x = cx;
    int tab[10];

    deduce1(x);
    auto ax1 = x; // int

    deduce1(cx);
    auto ax2 = cx; // int

    deduce1(ref_x);
    auto ax3 = ref_x; // int

    deduce1(cref_x);
    auto ax4 = cref_x; // int

    deduce1(tab);
    auto ax5 = tab; // int* - decay an array to a pointer

    deduce1(foo);
    auto ax6 = foo; // void(*ax6)() = foo; // decay a function to a pointer
}

TEST_CASE("Template Argument Deduction - case 2")
{
    int x = 10;
    const int cx = 20;
    int& ref_x = x;
    const int& cref_x = cx;
    int tab[10];

    deduce2(x);
    auto& ax1 = x; // int&

    deduce2(cx);
    auto& ax2 = cx; // const int&

    deduce2(ref_x);
    auto& ax3 = ref_x; // int&

    deduce2(cref_x);
    auto& ax4 = cref_x; // const int&

    deduce2(tab);
    auto& ax5 = tab; //int(&)[10]

    deduce2(foo);
    auto& ax6 = foo;
}

TEST_CASE("Template Argument Deduction - case 3")
{
    int x = 10;

    deduce3(x);
    auto&& ax1 = x; // int&

    deduce3(10);
    auto&& ax2 = 10; // int&&
}

template <typename T1, typename T2>
struct Pair
{
    T1 fst;
    T2 snd;

    template <typename Arg1, typename Arg2>
    Pair(Arg1&& f, Arg2&& s)
        : fst(std::forward<Arg1>(f))
        , snd(std::forward<Arg2>(s))
    {
    }

    Pair(const std::pair<T1, T2>& p)
        : fst {p.first}
        , snd {p.second}
    {
    }
};

// deduction guide
template <typename T1, typename T2>
Pair(T1, T2)->Pair<T1, T2>;

Pair(const char*, const char*)->Pair<std::string, std::string>;

TEST_CASE("CTAD - class template  argument deduction")
{
    Pair<int, double> p1 {1, 3.14};

    Pair p2 {1, 3.14}; // Pair<int, double>
    static_assert(is_same_v<decltype(p2), Pair<int, double>>);

    Pair p3 {"test"s, "text"}; // Pair<string, const char*>

    Pair p4 {foo, p3}; // Pair<void(*)(), Pair<string, const char*>>

    std::pair p5 {3.14, "pi"}; // std::pair<double, const char*>

    Pair p6 {p5}; //Pair<double, const char*>

    std::vector vec = {1, 2, 3, 4, 5};
    std::sort(vec.begin(), vec.end(), std::greater {});

    Pair p7 {"abc", "def"};
}

TEST_CASE("CTAD - deduce when copy")
{
    vector v1 {1, 2, 3, 4}; // vector<int>
    vector v2 {1}; // vector<int>

    SECTION("!!!")
    {
        vector v3 {v1}; // vector<int>
        static_assert(is_same_v<decltype(v3), vector<int>>);

        vector v4 {v1, v2};
        static_assert(is_same_v<decltype(v4), vector<vector<int>>>);

        optional o1 = 4; // optional<int>
        optional o2 = o1; // optional<int>
    }
}

template <typename T>
struct Generic
{
    T value;

    using Type = common_type_t<T>;

    Generic(Type v)
        : value {v}
    {
    }
};

template <typename TArg>
Generic(TArg)->Generic<enable_if_t<is_arithmetic_v<TArg>, TArg>>;

TEST_CASE("disabling CTAD")
{
    Generic g1 {1}; // Generic<int>
    Generic g2 = 3.14; // Generic<double>
    //Generic g3 = "text"s; // Generic<string>
    //Generic g4 = "abc"; // Generic<const char*>
}

template <typename T, size_t N>
struct Array
{
    T items[N];

    constexpr size_t size() const
    {
        return N;
    }
};

template <typename Head, typename... Tail>
Array(Head, Tail...)->Array<enable_if_t<(is_same_v<Head, Tail> && ...), Head>, sizeof...(Tail) + 1>;

TEST_CASE("aggregates + deduction guides")
{
    Array arr {1, 2, 3, 4};

    static_assert(is_same_v<decltype(arr), Array<int, 4>>);

    std::array coord {1, 2, 3};
}

TEST_CASE("CTAD in std library")
{
    SECTION("pair")
    {
        pair p {1, "text"s};

        const int x = 10;
        pair p2 {1, x}; // pair<int, int>
    }

    SECTION("tuple")
    {
        tuple t1 {1, 3.14, "text"s}; // tuple<int, double, string>
        tuple t2 {pair {1, "text"s}}; // tuple<int, string>
    }

    SECTION("optional")
    {
        optional opt = "text"s; // optional<string>

        optional opt2 = opt; // optional<string>
    }

    SECTION("containers")
    {
        vector vec = {1, 2, 3, 4};
        list lst = {1, 2, 3, 4};

        vector backup(begin(lst), end(lst)); // vector<int>

        array arr = {1, 2, 3, 4, 5}; // array<int, 5>
    }

    SECTION("function")
    {
        function f = foo; // function<void()>
        f();
    }

    SECTION("smart ptrs")
    {
        unique_ptr<int> ptr(new int(13));

        shared_ptr sptr = move(ptr);
        weak_ptr wptr = sptr;
        shared_ptr other(wptr);
    }

    SECTION("functors")
    {
        vector vec = {534, 23436, 12, 53454, 24365, 234};
        sort(vec.begin(), vec.end(), greater {}); // greater<> -> greate<void>
    }
}

template <typename... Lambdas>
struct Overload : Lambdas...
{
    using Lambdas::operator()...;
};

template <typename... Lambdas>
Overload(Lambdas...)->Overload<Lambdas...>;

struct Printer
{
    void operator()(const string& item) { cout << "string: " << item << "\n"; }
    void operator()(int item) { cout << "int: " << item << "\n"; }
    void operator()(double item) { cout << "double: " << item << "\n"; }
};

TEST_CASE("overloader with lambda")
{
    auto printer = Overload {
        [](const string& item) { cout << "string: " << item << "\n"; },
        [](int item) { cout << "int: " << item << "\n"; },
        [](double item) { cout << "double: " << item << "\n"; }};

    printer("text"s);
    printer(42);
    printer(3.14);

    cout << "Overload type: " << boost::typeindex::type_id<decltype(printer)>().pretty_name() << endl;
}

template <typename T>
constexpr T pi = 3.141523523465734768876;

template <typename T>
struct Pi
{
    constexpr static T value = 3.141523523465734768876;
};

TEST_CASE("variable template")
{
    cout << pi<double> << endl;

    cout << pi<float> << endl;

    cout << pi<int> << endl;

    cout << Pi<double>::value << endl;

    int x;

    static_assert(is_integral_v<decltype(x)>);
}
