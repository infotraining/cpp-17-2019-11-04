#include <algorithm>
#include <charconv>
#include <iostream>
#include <numeric>
#include <string>
#include <string_view>
#include <vector>

#include "catch.hpp"

using namespace std;

TEST_CASE("optional")
{
    SECTION("constructors")
    {
        optional<int> opt1;
        REQUIRE_FALSE(opt1.has_value());

        optional<int> opt2 = 42;
        REQUIRE(opt2.has_value());

        optional opt3 = "text"s;
        REQUIRE(opt3.has_value());

        auto text = "abcdef";
        optional<string_view> opt4 {in_place, text, 3};
        REQUIRE(opt4.has_value());
    }

    SECTION("getting value")
    {
        optional o1 = 42;
        optional<int> o2;

        SECTION("unsafe")
        {
            REQUIRE(*o1 == 42);
            //REQUIRE(*o2); // UB
        }

        SECTION("safe")
        {
            REQUIRE(o1.value() == 42);
            REQUIRE_THROWS_AS(o2.value(), bad_optional_access);

            REQUIRE(o2.value_or(-1) == -1);
        }
    }
}

optional<int> to_int(string_view str)
{
    int result {};

    auto start = str.data();
    auto end = start + str.size();

    if (const auto& [pos, error_code] = from_chars(start, end, result); error_code != std::errc {} || pos != end)
    {
        return nullopt;
    }

    return result;
}

TEST_CASE("to_int")
{
    optional n = to_int("42");

    REQUIRE(n.has_value());
    REQUIRE(n.value() == 42);

    n = to_int("4ab");

    REQUIRE(n.has_value() == false);
}
