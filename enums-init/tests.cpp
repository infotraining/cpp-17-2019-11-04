#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

enum class Coffee : uint8_t
{
    espresso,
    latte,
    americano
};

enum class Index : size_t
{
};

constexpr size_t to_size_t(Index i)
{
    return static_cast<size_t>(i);
}

struct Array
{
    vector<int> items;

    int& operator[](Index index)
    {
        return items[to_size_t(index)];
    }
};

TEST_CASE("enums in C++17")
{
    Coffee c1 = Coffee::espresso;
    Coffee c2 = static_cast<Coffee>(0);
    Coffee c3 {0};

    Array arr {{1, 2, 3, 4}};

    REQUIRE(arr[Index {2}] == 3);
}

TEST_CASE("std::byte")
{
    char byte = 8;

    byte <<= 2;

    byte += 3; //??

    cout << "byte: " << byte << "\n";

    std::byte b1 {0b0000'0010};
    std::byte b2 {2};

    b1 <<= 2;
    b2 = ~b1 & b2;

    cout << "b1: " << to_integer<int>(b1) << "\n";
    cout << "b2: " << to_integer<int>(b2) << "\n";
}

TEST_CASE("auto & list initialization")
{
    SECTION("before C++17")
    {
        int x1 = 1;
        int x2 {1};

        auto ax1 = 1; // int
        auto ax2 {1}; // intizlizer_list<int>
        auto ax3 = {1}; // intizlizer_list<int>
    }

    SECTION("since C++17")
    {
        int x1 = 1;
        int x2 {1};

        auto ax1 = 1; // int
        auto ax2 {1}; // int
        static_assert(is_same_v<decltype(ax2), int>);

        auto ax3 = {1}; // initializer_list<int>
    }
}
