#include <algorithm>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <string>
#include <tuple>
#include <vector>

#include "catch.hpp"

using namespace std;

namespace Legacy
{
    tuple<int, int, double> calc_stats(const vector<int>& data)
    {
        vector<int>::const_iterator min_pos, max_pos;
        tie(min_pos, max_pos) = minmax_element(data.begin(), data.end());

        double avg = accumulate(data.begin(), data.end(), 0.0) / data.size();

        return make_tuple(*min_pos, *max_pos, avg);
    }
}

template <typename Container>
auto calc_stats(const Container& data)
{
    auto [min_pos, max_pos] = minmax_element(begin(data), end(data));

    double avg = accumulate(begin(data), end(data), 0.0) / size(data);

    return tuple(*min_pos, *max_pos, avg);
}

TEST_CASE("Since C++17")
{
    vector data = {4, 42, 665, 1, 123, 13};

    auto [min, max, avg] = calc_stats(data);

    REQUIRE(min == 1);
    REQUIRE(max == 665);
    REQUIRE(avg == Approx(141.333));       
}

auto get_coord() -> int (&)[3]
{
    static int pos[] = {1, 2, 3};

    return pos;
}

std::array<int, 3> get_velocity()
{
    return {1, 2, 3};
}

TEST_CASE("structured bindings")
{
    SECTION("binding with an array")
    {
        auto [x, y, z] = get_coord();

        REQUIRE(x == 1);
        REQUIRE(y == 2);
        REQUIRE(z == 3);
    }

    SECTION("binding with std::array")
    {
        auto [vx, vy, vz] = get_velocity();

        REQUIRE(vx == 1);
        REQUIRE(vy == 2);
        REQUIRE(vz == 3);
    }

    SECTION("binding with tuple-like object")
    {
        tuple tpl {1, 3.14, "text"s};
        auto [x, pi, text] = tpl;

        REQUIRE(x == 1);
        REQUIRE(text == "text"s);
    }
}

struct Error
{
    int ec;
    string msg;
};

[[nodiscard]] Error open_file()
{
    return {13, "file not found"};
}

TEST_CASE("structured binding - struct/class")
{
    auto [error_code, message] = open_file();

    REQUIRE(error_code == 13);
    REQUIRE(message == "file not found"s);
}

TEST_CASE("structured binding - how it works")
{
    tuple tpl {1, 3.14, "text"s};

    auto [x, pi, msg] = tpl;

    x = 2;
    pi = 6.28;

    REQUIRE(get<0>(tpl) == 1);
    REQUIRE(get<1>(tpl) == Approx(3.14));

    SECTION("is interpreted as")
    {
        auto unnamed_obj = tpl;

        auto& x = get<0>(unnamed_obj);
        auto& pi = get<1>(unnamed_obj);
        auto& text = get<2>(unnamed_obj);
    }
}

TEST_CASE("structure binding - using refs")
{
    tuple tpl {1, 3.14, "text"s};

    auto& [x, pi, msg] = tpl;

    SECTION("is interpreted as")
    {
        auto& unnamed_obj = tpl;

        auto& x = get<0>(unnamed_obj);
        auto& pi = get<1>(unnamed_obj);
        auto& text = get<2>(unnamed_obj);
    }

    x = 2;
    REQUIRE(get<0>(tpl) == 2);
}

TEST_CASE("&, const, volatile & alignas")
{
    vector vec = {1, 2, 3};

    const auto& [min, max, avg] = calc_stats(vec);

    alignas(16) auto [error_code, message] = open_file();
}

struct Data
{
    int x;
    int& ref_x;
};

TEST_CASE("beware - names are references")
{
    int x = 10;
    int& ref_x = x;

    auto ax1 = x; // int
    auto ax2 = ref_x; // int

    SECTION("broken deduction")
    {
        Data data {x, ref_x};

        auto [ax1, ax2] = data;

        static_assert(is_same_v<decltype(ax1), int>);
        static_assert(is_same_v<decltype(ax2), int&>);
    }
}

TEST_CASE("use cases")
{
    map<int, string> dict = {{1, "one"}, {2, "two"}, {4, "four"}};

    SECTION("iteration over map")
    {
        for (const auto& [key, value] : dict)
            cout << key << " - " << value << "\n";
    }

    SECTION("insert into map")
    {
        if (auto [pos, was_inserted] = dict.insert(pair(3, "three")); was_inserted)
        {
            const auto& [key, value] = *pos;
            cout << "(" << key << ", " << value << ") was inserted\n";
        }
        else
        {
            const auto& [key, value] = *pos;
            cout << "(" << key << ", " << value << ") was already in container\n";
        }
    }

    SECTION("init many vars")
    {
        auto [x, y, is_ok] = tuple(1, 3.14, true);

        list lst = {1, 2, 3, 4, 5, 6};

        for (auto [index, it] = tuple(0, begin(lst)); it != end(lst); ++index, ++it)
        {
            cout << "item: " << *it << " at index " << index << "\n";
        }
    }
}

enum Something
{
    some,
    thing
};

const map<Something, string> something_desc = {{some, "some"}, {thing, "thing"}};

// step 1 - describe tuple-size of items
template <>
struct std::tuple_size<Something>
{
    static constexpr size_t value = 2;
};

// step 2 - describe types of items
template <>
struct std::tuple_element<0, Something>
{
    using type = int;
};

template <>
struct std::tuple_element<1, Something>
{
    using type = std::string;
};

// step 3 - implementing get(const Something&)

template <size_t>
decltype(auto) get(const Something& s);

template <>
decltype(auto) get<0>(const Something& s)
{
    return static_cast<int>(s);
}

template <>
decltype(auto) get<1>(const Something& s)
{
    return something_desc.at(s);
}

TEST_CASE("tuple-like protocol")
{
    Something sth = some;

    const auto [value, description] = sth;
    cout << "Var sth: " << value << " - " << description << "\n";
}
