#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

namespace BeforeCpp17
{
    template <typename Head>
    auto sum(Head head)
    {
        return head;
    }

    template <typename Head, typename... Tail>
    auto sum(Head head, Tail... tail)
    {
        return head + sum(tail...);
    }
}

template <typename... Args>
auto sum(Args... args)
{
    return (... + args); // return ((((1 + 2) + 3) + 4) + 5)
}

template <typename... Args>
auto sum_r(Args... args)
{
    return (args + ...); // return (1 + (2 + (3 + (4 + 5))))
}

TEST_CASE("unary fold expressions")
{
    auto result = sum(1, 2, 3, 4, 5);
    REQUIRE(result == 15);

    REQUIRE(sum(1, 2, 3) == sum_r(1, 2, 3));
}

template <typename... Args>
void print(const Args&... args)
{
    bool is_first = true;

    auto with_space = [&is_first](const auto& arg) {
        if (!is_first)
            cout << " ";
        is_first = false;
        return arg;
    };

    (cout << ... << with_space(args)) << "\n";
}

template <typename... Args>
void print_lines(const Args&... args)
{
    ((cout << args << "\n"), ...);
}

TEST_CASE("binary fold")
{
    print(1, 3.14, "text"s, "abc");
    print_lines(1, 3.14, "text"s, "abc");

    auto printer = [](const auto&... item) {
        (..., (cout << item << "\n"));
    };

    printer(1, 3.14, "text"s, "abc");
}
