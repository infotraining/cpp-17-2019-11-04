#ifndef GADGET_HPP
#define GADGET_HPP

#include <iostream>
#include <string>

class Gadget
{
    int id_;
    std::string name_;

    inline static int gen_id = 0;

public:
    Gadget(std::string name)
        : id_ {++gen_id}
        , name_ {std::move(name)}
    {
    }

    int id() const
    {
        return id_;
    }

    std::string name() const
    {
        return name_;
    }

    void use() const
    {
        std::cout << "Using gadget " << id_ << " - " << name_ << "\n";
    }
};

#endif // GADGET_HPP
