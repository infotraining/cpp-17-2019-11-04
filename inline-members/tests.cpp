#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"
#include "gadget.hpp"

using namespace std;

TEST_CASE("inline static members")
{
    Gadget g1 {"ipad"};
    Gadget g2 {"ipod"};

    g1.use();
    g2.use();
}
