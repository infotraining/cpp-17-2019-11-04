#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

vector<int> create_vector_rvo(size_t n) // rvo (mandatory in c++17)
{
    return vector<int>(n);
}

vector<int> create_vector_nrvo(size_t n) // named rvo (n-rvo) (optional in c++17)
{
    vector<int> vec(n);

    iota(begin(vec), end(vec), 0);

    return vec;
}

TEST_CASE("rvo")
{
    vector<int> vec = create_vector_rvo(1'000'000); // copy ellision & rvo
    vector<int> data = create_vector_nrvo(1'000'000);
}

struct CopyMoveDisabled
{
    vector<int> data;

    CopyMoveDisabled(std::initializer_list<int> lst)
        : data {lst}
    {
    }

    CopyMoveDisabled(const CopyMoveDisabled&) = delete;
    CopyMoveDisabled& operator=(const CopyMoveDisabled&) = delete;
    CopyMoveDisabled(CopyMoveDisabled&&) = delete;
    CopyMoveDisabled& operator=(CopyMoveDisabled&&) = delete;
};

CopyMoveDisabled create_impossible()
{
    return CopyMoveDisabled {1, 2, 3};
}

void use(CopyMoveDisabled arg)
{
    cout << "Using object with data: " << arg.data.size() << "\n";
}

TEST_CASE("create impossible")
{
    CopyMoveDisabled cmd = create_impossible();

    REQUIRE(cmd.data == vector {1, 2, 3});

    use(create_impossible());
    use(CopyMoveDisabled {6, 7, 8, 9, 10});
}
