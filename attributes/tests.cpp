#include <algorithm>
#include <future>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

[[nodiscard]] future<void> save_file(const string& filename)
{
    auto download = [=] {
        cout << "Start saving " << filename << "..." << endl;

        this_thread::sleep_for(2s);

        cout << filename << " saved..." << endl;
    };

    return async(launch::async, download);
}

TEST_CASE("nodiscard")
{
    {
        auto f1 = save_file("a.dat");
        auto f2 = save_file("b.dat");
        auto f3 = save_file("c.dat");
        auto f4 = save_file("d.dat");
    }
}

TEST_CASE("maybe_unused")
{
    [[maybe_unused]] int x = 10;
}

TEST_CASE("fallthrough")
{
    int x = 2;

    switch (x)
    {
    case 1:
        cout << "one\n";
        break;
    case 2:
        [[fallthrough]];
    case 3:
        cout << "two or four\n";
        break;
    default:
        cout << "> 3\n";
        break;
    }
}

namespace Lib
{
    namespace [[deprecated]] ver_1
    {
        void foo()
        {
            cout << "foo() ver 1\n";
        }
    }

    namespace ver_2
    {
        void foo()
        {
            cout << "foo() ver 1\n";
        }
    }
}

enum Coffee
{
    espresso = 1,
    americano [[deprecated]] = espresso
};

TEST_CASE("using lib")
{
    Lib::ver_2::foo();

    Coffee c = espresso;

    static_assert(sizeof(int) >= 4);
}
