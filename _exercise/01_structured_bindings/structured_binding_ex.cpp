#include <iostream>
#include <string>

#include "catch.hpp"

using namespace std;

class Customer
{
private:
    std::string first_;
    std::string last_;
    uint8_t age_;

public:
    Customer(std::string f, std::string l, uint8_t age)
        : first_(std::move(f))
        , last_(std::move(l))
        , age_(age)
    {
    }

    std::string first() const
    {
        return first_;
    }

    std::string last() const
    {
        return last_;
    }

    uint8_t age() const
    {
        return age_;
    }
};

// step 1
template <>
struct std::tuple_size<Customer> : std::integral_constant<size_t, 3>
{
};

// step 2
template <>
struct std::tuple_element<0, Customer>
{
    using type = std::string;
};

template <>
struct std::tuple_element<1, Customer> : std::common_type<std::string>
{
};

template <>
struct std::tuple_element<2, Customer> : std::common_type<uint8_t>
{
};

// step 3
template <size_t N>
decltype(auto) get(const Customer& c)
{
    if constexpr (N == 0)
    {
        return c.first();
    }
    else if constexpr (N == 1)
    {
        return c.last();
    }
    else
        return c.age();
}

//template <>
//decltype(auto) get<0>(const Customer& c)
//{
//    return c.first();
//}

//template <>
//decltype(auto) get<1>(const Customer& c)
//{
//    return c.last();
//}

//template <>
//decltype(auto) get<2>(const Customer& c)
//{
//    return c.age();
//}

TEST_CASE("decompose using structure bindings")
{
    Customer c("Jan", "Kowalski", 42);

    auto [fn, ln, a] = c;

    REQUIRE(fn == "Jan");
    REQUIRE(ln == "Kowalski");
    REQUIRE(a == 42);
}
