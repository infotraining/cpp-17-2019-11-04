#include <algorithm>
#include <iostream>
#include <mutex>
#include <numeric>
#include <optional>
#include <queue>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

TEST_CASE("if with initializer")
{
    vector vec = {5, 245, 234, 42, 465, 6745, 665, 34654};

    if (auto pos = find(begin(vec), end(vec), 42); pos != end(vec))
    {
        cout << "Item " << *pos << " has been found!\n";
    }
    else
    {
        assert(pos == end(vec));
    }
}

TEST_CASE("use case with mutex")
{
    queue<int> q;
    mutex mtx_q;

    q.push(42);

    SECTION("before C++17")
    {
        auto pop = [&q, &mtx_q] {
            {
                lock_guard<mutex> lk {mtx_q};

                if (!q.empty())
                {
                    auto value = q.front();
                    q.pop();

                    return optional(value);
                }
            }

            return optional<int>(nullopt);
        };

        auto item = pop();

        REQUIRE(*item == 42);
    }

    SECTION("since C++17")
    {
        auto pop = [&q, &mtx_q] {
            if (lock_guard lk {mtx_q}; !q.empty())
            {
                auto value = q.front();
                q.pop();

                return optional(value);
            }

            return optional<int>(nullopt);
        };

        auto item = pop();

        REQUIRE(*item == 42);
    }
}
