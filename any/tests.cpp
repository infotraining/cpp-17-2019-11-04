#include <algorithm>
#include <any>
#include <iostream>
#include <map>
#include <numeric>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

TEST_CASE("any")
{
    any anything;

    REQUIRE(anything.has_value() == false);

    anything = 1;
    anything = 3.14;
    anything = "text"s;
    anything = vector {1, 2, 3, 4};

    SECTION("any_cast")
    {
        auto vec = any_cast<vector<int>>(anything);

        REQUIRE(vec == vector {1, 2, 3, 4});

        REQUIRE_THROWS_AS(any_cast<string>(anything), bad_any_cast);
    }

    SECTION("any_cast with pointers")
    {
        vector<int>* ptr_vec = any_cast<vector<int>>(&anything);

        REQUIRE(ptr_vec != nullptr);
        REQUIRE(*ptr_vec == vector {1, 2, 3, 4});

        REQUIRE(any_cast<string>(&anything) == nullptr);
    }

    SECTION("type + RTTI")
    {
        const type_info& type_desc = anything.type();

        cout << type_desc.name() << endl;

        REQUIRE(anything.type() == typeid(vector<int>));
    }
}

class KeyValueDictionary
{
    map<string, any> dict_;

public:
    optional<map<string, any>::iterator> insert(string key, any value)
    {
        auto [pos, was_inserted] = dict_.emplace(move(key), move(value));

        if (was_inserted)
            return pos;
        return nullopt;
    }

    template <typename T>
    T& at(const string& key)
    {
        T* value = any_cast<T>(&dict_.at(key));

        if (!value)
            throw bad_any_cast();

        return *value;
    }
};

TEST_CASE("KeyValueDictionary")
{
    KeyValueDictionary dict;

    dict.insert("name", "jan"s);
    dict.insert("age", 33);
    dict.insert("data", vector {1, 3, 4});

    REQUIRE(dict.at<int>("age") == 33);
    REQUIRE(dict.at<vector<int>>("data") == vector {1, 3, 4});
    REQUIRE_THROWS_AS(dict.at<int>("name"), bad_any_cast);
}

class TempMonitor
{
public:
    double get_temp()
    {
    }
};

class Observer
{
public:
    virtual void update(std::any sender, const string& msg) = 0;
    virtual ~Observer() = default;
};

class Logger : public Observer
{
public:
    void update(std::any sender, const string& msg) override
    {
        TempMonitor* monitor = any_cast<TempMonitor>(&sender);
        if (monitor)
            monitor->get_temp();
    }
};
