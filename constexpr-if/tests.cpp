#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

template <typename T>
string convert_to_string(T value)
{
    if constexpr (is_arithmetic_v<T>)
        return to_string(value);
    else if constexpr (is_same_v<T, string>)
        return value;
    else
        return string(value);
}

TEST_CASE("constexpr if")
{
    SECTION("convert to string")
    {
        SECTION("from number")
        {
            string str = convert_to_string(42);

            REQUIRE(str == "42"s);
        }

        SECTION("from string")
        {
            string str = convert_to_string("42"s);

            REQUIRE(str == "42"s);
        }

        SECTION("from const char*")
        {
            string str = convert_to_string("abc");

            REQUIRE(str == "abc"s);
        }

        SECTION("from string_view")
        {
            string str = convert_to_string("abc"sv);

            REQUIRE(str == "abc"s);
        }
    }
}

template <typename T, size_t N>
auto process_array(T (&data)[N])
{
    if constexpr (N <= 255)
    {
        cout << "Processing small array\n";
        std::array<T, N> result;
        copy(begin(data), end(data), begin(result));
        return result;
    }
    else
    {
        cout << "Processing large array\n";
        std::vector<T> result(N);
        copy(begin(data), end(data), begin(result));
        return result;
    }
}

TEST_CASE("processing arrays")
{
    int small_buffer[16] = {1, 2, 3, 4};
    auto backup1 = process_array(small_buffer);
    static_assert(is_same_v<decltype(backup1), std::array<int, 16>>);

    int large_buffer[1024];
    auto backup2 = process_array(large_buffer);
    static_assert(is_same_v<decltype(backup2), std::vector<int>>);
}

namespace BeforeCpp17
{
    void print()
    {
        cout << "\n";
    }

    template <typename Head, typename... Tail>
    void print(const Head& head, const Tail&... tail)
    {
        cout << head << " ";
        print(tail...);
    }
}

template <typename Head, typename... Tail>
void print(const Head& head, const Tail&... tail)
{
    cout << head << " ";

    if constexpr (sizeof...(tail) > 0)
        print(tail...);
    else
        cout << "\n";
}

TEST_CASE("variadic print")
{
    print(1, 3.14, "text"s, "hello");
}
