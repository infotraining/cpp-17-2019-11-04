#include <algorithm>
#include <iostream>
#include <numeric>
#include <string>
#include <string_view>
#include <vector>

#include "catch.hpp"

using namespace std;

TEST_CASE("string_view")
{
    const char* ctext = "text";
    string_view sv1 = ctext;
    cout << sv1 << endl;
    REQUIRE(sv1.size() == 4);

    string str = "hello"s;
    string_view sv2 = str;
    REQUIRE(sv2 == "hello"sv);

    const char* words = "abc def ghi";
    string_view token1(words, 3);
    string_view token2(words + 4, 3);

    REQUIRE(token1 == "abc"sv);
    REQUIRE(token2 == "def"sv);
}

template <typename Container>
void print_all(const Container& container, string_view prefix)
{
    cout << prefix << " : [ ";
    for (const auto& item : container)
        cout << item << " ";
    cout << "]\n";
}

TEST_CASE("print all")
{
    vector vec = {1, 2, 3, 4, 5, 6};

    print_all(vec, "items of vec");
    string text = "vec";
    print_all(vec, text);
    print_all(vec, "items of vec"s);
}

TEST_CASE("beware - unsafe code")
{
    string_view sv = "text"s; // UB

    int tab[] = {423, 5346, 65, 34645, 34};

    cout << sv << "\n";
}

string_view get_prefix(string_view text, size_t length)
{
    return {text.data(), length};
}

TEST_CASE("get prefix")
{
    string text = "abcdef";

    auto prefix = get_prefix(text, 3);

    cout << prefix << "\n";

    prefix = get_prefix("abcdef"s, 3);

    int tab[] = {423, 5346, 65, 34645, 34};

    cout << prefix << "\n";
}

TEST_CASE("string_view -> string")
{
    auto text = "abc"sv;

    string str(text);

    REQUIRE(str == "abc"s);
}

TEST_CASE("default constructor")
{
    string_view sv;

    REQUIRE(sv.data() == nullptr);
}

TEST_CASE("string_view is not null-terminated")
{
    const char text[4] = {'a', 'b', 'c', 'd'};

    string_view sv(text, 4);

    cout << sv << endl;
}

namespace Cpp20
{
    template <typename Iterator, typename Predicate>
    constexpr Iterator find_if(Iterator first, Iterator last, Predicate pred)
    {
        for (Iterator it = first; it != last; ++it)
            if (pred(*it))
                return it;

        return last;
    }
}

template <auto N>
constexpr optional<string_view> get_id(const std::array<std::string_view, N>& data, string_view id)
{
    auto it = Cpp20::find_if(begin(data), end(data), [id](auto sv) { return sv == id; });

    if (it != end(data))
        return *it;

    return std::nullopt;
}

TEST_CASE("constexpr")
{
    constexpr string_view text = "abcdef"sv;
    constexpr string_view subtext(text.data() + 3, 3);
    static_assert(subtext == "def"sv);

    constexpr std::array ids = {"aa"sv, "bb"sv, "cc"sv, "dd"sv};
    constexpr optional opt_id = get_id(ids, "cc"sv);

    static_assert(opt_id.has_value());
    static_assert(opt_id.value() == "cc"sv);
}
