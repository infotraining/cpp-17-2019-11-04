#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

struct SimpleAggregate
{
    int a;
    double b;
    int data[3];
    string name;

    void print() const
    {
        cout << "SimpleAggregate{ " << a << ", " << b << ", [ ";
        for (const auto& item : data)
            cout << item << " ";
        cout << "], " << name << "}\n";
    }
};

struct Point
{
    int x, y;

    //    Point(int x, int y)
    //        : x(x)
    //        , y(y)
    //    {
    //    }
};

TEST_CASE("C++98")
{
    Point pt {1, 2};
}

TEST_CASE("aggregates")
{
    SimpleAggregate agg1 {1, 3.14, {1, 2, 3}, "agg1"};
    agg1.print();

    SimpleAggregate agg2 {};
    agg2.print();

    SimpleAggregate agg3 {1, 3.14};
    agg3.print();

    static_assert(is_aggregate_v<SimpleAggregate>);
    static_assert(is_aggregate_v<Point>);
}

TEST_CASE("arrays are aggregates")
{
    int tab1[3] = {1, 2, 3};
    int tab2[3] = {};
    int tab3[3] = {1, 2};
}

struct ComplexAggregate : SimpleAggregate, std::string
{
    vector<int> vec;
};

TEST_CASE("complex aggregate")
{
    ComplexAggregate agg1 {{1, 3.14, {1, 2, 3}, "base"}, {"text"}, {1, 2, 3, 4, 5}};

    agg1.print();
    REQUIRE(agg1.size() == 4);
    REQUIRE(agg1.vec == vector {1, 2, 3, 4, 5});

    static_assert(is_aggregate_v<std::array<int, 4>>);

    std::array<int, 4> arr = {1, 2};
}

struct WTF
{
    int value;

    WTF() = delete;
};

TEST_CASE("WTF")
{
    WTF wtf {};

    REQUIRE(wtf.value == 0);
}
