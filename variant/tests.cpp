#include <algorithm>
#include <any>
#include <iostream>
#include <list>
#include <numeric>
#include <string>
#include <variant>
#include <vector>

#include "catch.hpp"

using namespace std;

TEST_CASE("variant")
{
    variant<int, double, string> v1;

    REQUIRE(holds_alternative<int>(v1));
    REQUIRE(get<int>(v1) == 0);

    v1 = 3.14;
    v1 = "text"s;

    REQUIRE(v1.index() == 2);
    REQUIRE(get<string>(v1) == "text"s);
    REQUIRE_THROWS_AS(get<int>(v1), bad_variant_access);

    REQUIRE(get_if<int>(&v1) == nullptr);
    REQUIRE(*get_if<string>(&v1) == "text"s);
}

TEST_CASE("duplicates on type list")
{
    variant<int, double, int> strange;

    REQUIRE(strange.index() == 0);
    REQUIRE(get<0>(strange) == 0);

    strange = 3.14;
    strange.emplace<2>(42);

    REQUIRE(strange.index() == 2);
    REQUIRE(get<2>(strange) == 42);
}

struct Data
{
    vector<int> data;

    Data(initializer_list<int> lst)
        : data {lst}
    {
    }
};

TEST_CASE("")
{
    variant<monostate, Data, string> v1;

    REQUIRE(holds_alternative<monostate>(v1));
}

struct Value
{
    float v;

    Value(float v)
        : v {v}
    {
    }

    ~Value()
    {
        cout << "~Value(" << v << ")\n";
    }
};

struct Evil
{
    string v;

    Evil(string v)
        : v {move(v)}
    {
    }

    Evil(Evil&& other)
    {
        throw 42;
    }
};

TEST_CASE("valueless variant")
{
    cout << "--------------------\n";
    variant<Value, Evil> v {12.f};

    try
    {
        v.emplace<1>(Evil {"evil"});
    }
    catch (...)
    {
        cout << "--------------------\n";
    }

    REQUIRE(v.valueless_by_exception());
    REQUIRE(v.index() == variant_npos);
}

struct Printer
{
    void operator()(int x) const
    {
        cout << "int: " << x << "\n";
    }

    void operator()(const string& str) const
    {
        cout << "string: " << str << "\n";
    }

    void operator()(const vector<int>& vec) const
    {
        cout << "vector: [ ";
        for (const auto& item : vec)
            cout << item << " ";
        cout << "]\n";
    }
};

template <typename... Ts>
struct Overload : Ts...
{
    using Ts::operator()...;
};

template <typename... Ts>
Overload(Ts...)->Overload<Ts...>;

TEST_CASE("visiting")
{
    variant<int, string, vector<int>, double> v;

    v = vector {1, 2, 3};
    v = 3.14;

    SECTION("unsafe visitation")
    {
        if (holds_alternative<int>(v))
        {
            cout << "int: " << *get_if<int>(&v) << "\n";
        }
        else if (holds_alternative<string>(v))
        {
            cout << "string: " << *get_if<string>(&v) << "\n";
        }
    }

    SECTION("safe visit")
    {
        Printer prn;

        visit(prn, v);
    }

    SECTION("visit inplace")
    {
        string prefix = "# ";

        auto printer = Overload {
            [](int x) { cout << "visiting int: " << x << "\n"; },
            [](const string& s) { cout << "visiting string: " << s << "\n"; },
            [prefix](const vector<int>& v) { cout << prefix << "visiting vector: v.size() = " << v.size() << "\n"; },
            [](const auto& other) { cout << "visiting unknown type: " << other << "\n"; }};

        visit(printer, v);

        variant<int, string> v2 = "text"s;
        visit(printer, v2);

        auto ext_printer = Overload {
            printer,
            [](const any& a) { cout << "visiting any: " << a.type().name() << "\n"; }};

        variant<int, double, any> v3 = 3.14;
        v3 = list {1, 2, 3};
        v3 = 42;
        v3 = 3.14;

        REQUIRE(v3.index() == 1);

        visit(ext_printer, v3);
    }
}

struct ErrorCode
{
    string message;
    std::errc error_code;
};

[[nodiscard]] variant<string, ErrorCode> load_from_file(const string& filename)
{
    if (filename == "unknown")
    {
        return ErrorCode {"Error#13", errc::bad_file_descriptor};
    }

    return "Content of "s + filename;
}

TEST_CASE("handling resulsts or errors")
{
    visit(Overload {
              [](const string& content) { cout << content << "\n"; },
              [](const ErrorCode& ec) { cout << ec.message << "\n"; }},
        load_from_file("unknown"));
}
